import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';
import {Viewer} from "@microsoft/sarif-web-component";

(async () => {
    const sarif = await fetch('./semgrep.sarif')
        .then(response => response.json());

    console.log(sarif);

    const root = document.getElementById('root') as HTMLElement
    ReactDOM.render(<Viewer logs={[sarif]} />, root);
})();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
